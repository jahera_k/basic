<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Category;
use Auth;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;

class CategoryController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    Public function AllCat()
    {
        $categories=Category::latest()->paginate(5);
        $trashCat=Category::onlyTrashed()->latest()->paginate(3);
        // $categories=DB::table('categories')->latest()->paginate(5);
        // $categories=DB::table('categories')
        //                ->join('users','categories.user_id','users.id')
        //                ->select('categories.*','users.name')
        //                ->latest()->paginate(5);

        return view('admin.category.index',compact('categories','trashCat'));

    }
    Public function AddCat(Request $request)
    {
        $validated = $request->validate([
            'category_name' => 'required|unique:categories|max:255',
          
        ],[
            'category_name.required'=>'please input category name',
            'category_name.max'=>'category should less than 255 chars',

        ]);
        // Category::insert([
        //     'category_name'=>$request->category_name,
        //     'user_id'=>Auth::user()->id,
        //     'created_at'=>Carbon::now(),
        // ]);
        $data=array();
        $data['category_name']=$request->category_name;
        $data['user_id']=Auth::user()->id;
        DB::table('categories')->insert($data);

        // $category=new Category;
        // $category->category_name=$request->category_name;
        // $category->user_id=Auth::user()->id;
        // $category->save();
        return redirect()->back()->with('success','category inserted successfully');

    }
    public function Edit($id)
    {
        // $categories=Category::find($id);
        $categories=DB::table('categories')->where('id',$id)->first();
        return view('admin.category.edit',compact('categories'));
    }
    public function Update(Request $request,$id)
    {
        // $update=Category::find($id)->update([
        //     'category_name'=>$request->category_name,
        //     'user_id'=>Auth::user()->id
        // ]);
        $data=array();
        $data['category_name']=$request->category_name;
        $data['user_id']=Auth::user()->id;
        DB::table('categories')->where('id',$id)->update($data);

        return redirect()->route('all.category')->with('success','Category updated successfully');

    }
    public function SoftDelete($id)
    {
        $delete=Category::find($id)->delete();
        return redirect()->back()->with('success','Category soft delete successfully');
    }
    public function Restore($id)
    {
      $delete=Category::withTrashed()->find($id)->restore();
      return redirect()->back()->with('success','Category Restore successfully');
    }
    public function PDelete($id)
    {
        $delete=Category::onlyTrashed()->find($id)->forceDelete();
        return redirect()->back()->with('success','Category permanently deleted successfully');

    }
}



