<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\About;

class AboutController extends Controller
{
    public function HomeAbout()
    {
        $homeabout=About::latest()->get();
        return view('admin.home.index',compact('homeabout'));
    }
}
