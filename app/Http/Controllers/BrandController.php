<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Brand;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Image;
use App\Models\Multipic;
use Auth;



class BrandController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function AllBrand()
    {
        $brands=Brand::latest()->paginate(5);
        return view('admin.brand.index',compact('brands'));
    }
    public function AddBrand(Request $request)
    {
        $validated = $request->validate([
            'brand_name' => 'required|unique:brands|min:3',
            'brand_image' => 'required|mimes:jpg,jpeg,png',

        ],[
            'brand_name.required'=>'please input brand name',
            'brand_image.min'=>'Brand longer than 6 characters',

        ]);
        $brand_image=$request->file('brand_image');
        
        // $name_gen=hexdec(uniqid());
        // $img_ext=strtolower($brand_image->getClientOriginalExtension());
        // $img_name=$name_gen.'.'.$img_ext;
        // $up_location='image/brand/';
        // $last_img=$up_location.$img_name;
        // $brand_image->move($up_location,$img_name);
 
        $name_gen=hexdec(uniqid()).'.'.$brand_image->getClientOriginalExtension();
        Image::make($brand_image)->resize(300,200)->save('image/brand/'.$name_gen);
        $last_img='image/brand/'.$name_gen;

        Brand::insert([
            'brand_name'=>$request->brand_name,
            'brand_image'=>$last_img,
            'created_at'=>Carbon::now(),
        ]);

        return redirect()->back()->with('success','Brand inserted successfully');

    }
    public function Edit($id)
    {
        // $brands=brand::find($id);
        $brands=DB::table('brands')->where('id',$id)->first();
        return view('admin.brand.edit',compact('brands'));
    }
    public function Update(Request $request,$id)
    {
        $validated = $request->validate([
            'brand_name' => 'required|min:3',

        ],[
            'brand_name.required'=>'please input brand name',
            'brand_image.min'=>'Brand longer than 6 characters',

        ]);
        $old_image=$request->old_image;
        
        $brand_image=$request->file('brand_image');
        if($brand_image)
    {
        $name_gen=hexdec(uniqid());
        $img_ext=strtolower($brand_image->getClientOriginalExtension());
        $img_name=$name_gen.'.'.$img_ext;
        $up_location='image/brand/';
        $last_img=$up_location.$img_name;
        $brand_image->move($up_location,$img_name);
        unlink($old_image);
        Brand::find($id)->update([
            'brand_name'=>$request->brand_name,
            'brand_image'=>$last_img,
            'created_at'=>Carbon::now(),
        ]);
        return redirect()->back()->with('success','Brand inserted successfully');
            
    }
    else{
        Brand::find($id)->update([
            'brand_name'=>$request->brand_name,
            'created_at'=>Carbon::now(),
            
        ]);
        return redirect()->back()->with('success','Brand inserted successfully');

    }

    }
    public function Delete($id)
    {
        $image=Brand::find($id);
        $old_image=$image->brand_image;
        unlink($old_image);
        Brand::find($id)->delete();
        return redirect()->back()->with('success','Brand delete successfully');

    }

  ////multi image part  
    public function Multipic()
    {
        $images=Multipic::all();
     return view('admin.multipic.index',compact('images'));
    }


    public function AddImage(Request $request)
    {
        $image=$request->file('image');
     foreach($image as $multi_image)
      {
        $name_gen=hexdec(uniqid()).'.'.$multi_image->getClientOriginalExtension();
        Image::make($multi_image)->resize(300,300)->save('image/multi/'.$name_gen);
        $last_img='image/multi/'.$name_gen;

        Multipic::insert([
            'image'=>$last_img,
            'created_at'=>Carbon::now(),
        ]);
      }
             return redirect()->back()->with('success','Images inserted successfully');

    }

    ///backend  

    public function Logout()
    {
        Auth::logout();
        return redirect()->route('login')->with('success','user logout');
    }
}
