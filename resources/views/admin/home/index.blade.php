@extends('admin.admin_master')
@section('admin')

<div class="container">
        @if(session('success'))
          <div class="alert alert-success" role="alert">
              <strong>{{session('success')}}</strong>
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
          </div>
        @endif
  <div class="row">
    <div class="col-md-12 mt-4">
      <a href=""><button class="btn btn-info float-right">Add About Data</button></a>
    </div> 
    <div class="col-md-12 mt-4">
        <div class="card-header">All About</div>
          <table class="table">
            <thead>
              <tr>
                <th class="col">Sl NO</th>
                <th class="col">Title</th>
                <th class="col">Long Description</th>
                <th class="col">Short Description</th>
                <th class="col">Action</th>
              </tr>
            </thead>
            <tbody>
            @php($i=1)
              @foreach($homeabout as $about)
              <tr>
                <th scope="row">{{$i++}}</th>
                 <td>{{$about->title}}</td>
                 <td>{{$about->short_dis}}</td>
                 <td>{{$about->long_dis}}</td>
                  <td>
                    <a href="{{url('about/edit/'.$about->id)}}" class="btn btn-info">Edit</a>
                    <a href="{{url('about/delete/'.$about->id)}}" onclick="return confirm('Are you sure to delete')"
                     class="btn btn-danger">Delete</a>
                  </td> 
               </tr>
               @endforeach
            </tbody>
          </table>  
          
      </div>  
    
  
</div>    

@endsection
