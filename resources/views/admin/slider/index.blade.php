@extends('admin.admin_master')
@section('admin')

<div class="container">
        @if(session('success'))
          <div class="alert alert-success" role="alert">
              <strong>{{session('success')}}</strong>
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
          </div>
        @endif
  <div class="row">
    <div class="col-md-12 mt-4">
      <a href="{{route('add.slider')}}"><button class="btn btn-info float-right">Add Slider</button></a>
    </div> 
    <div class="col-md-12 mt-4">
        <div class="card-header">All Slider</div>
          <table class="table">
            <thead>
              <tr>
                <th class="col-md-1">Sl NO</th>
                <th class="col-md-2">Title</th>
                <th class="col-md-5">Description</th>
                <th class="col-md-2">Image</th>
                <th class="col-md-2">Action</th>
              </tr>
            </thead>
            <tbody>
            @php($i=1)
              @foreach($sliders as $slider)
              <tr>
                <th scope="row">{{$i++}}</th>
                 <td>{{$slider->title}}</td>
                 <td>{{$slider->description}}</td>
                 <td><img src="{{asset($slider->image)}}" style="height:40px;width:70px;" ></td>
                
                  <td>
                    <a href="{{url('slider/edit/'.$slider->id)}}" class="btn btn-info">Edit</a>
                    <a href="{{url('slider/delete/'.$slider->id)}}" onclick="return confirm('Are you sure to delete')"
                     class="btn btn-danger">Delete</a>
                  </td> 
               </tr>
               @endforeach
            </tbody>
          </table>  
          
      </div>  
    
  
</div>    

@endsection
