<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
          Edit category
     
        </h2>
    </x-slot>

    <div class="py-12">
        
    </div>
<div class="container">
  <div class="row"> 
    <div class="col-md-4">
      <div class="card">
        <div class="card-header">Edit Category</div>
          @if(session('success'))
          <div class="alert alert-success" role="alert">
              <strong>{{session('success')}}</strong>
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
          </div>
          @endif
          <div class="card-body">
              <form action="{{url('category/update/'.$categories->id)}}" method="POST">
                @csrf
                <div class="form-group">
                  <label >Category name</label>
                  <input type="text" class="form-control" name="category_name" aria-describedby="emailHelp" 
                  placeholder="Enter category" value="{{$categories->category_name}}">
                   @error('category_name')
                   <span class="text-danger">{{$message}}</span>

                   @enderror
                </div>
                <button type="submit" class="btn btn-primary">Update</button>
              </form>
        </div>
    </div>  
  </div>
</div>    
</x-app-layout>
