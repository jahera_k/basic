<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
          All category
     
        </h2>
    </x-slot>

<br><br>

<div class="container">
        @if(session('success'))
          <div class="alert alert-success" role="alert">
              <strong>{{session('success')}}</strong>
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
          </div>
        @endif
  <div class="row">
    <div class="col-md-8">
        <div class="card-header">All Category</div>
          <table class="table">
            <thead>
              <tr>
                <th scope="col">Sl NO</th>
                <th scope="col">category  Name</th>
                <th scope="col">User Name</th>
                <th scope="col">Created At</th>
                <th scope="col">Action</th>
              </tr>
            </thead>
            <tbody>
            <!-- @php($i=1) -->
              @foreach($categories as $category)
              <tr>
                <th scope="row">{{$categories->firstItem()+$loop->index}}</th>
                 <td>{{$category->category_name}}</td>
                 <td>{{$category->user->name}}</td>
                 <td>
                   @if($category->created_at==NULL)
                   <span class="text-danger">No Date Set</span>
                   @else
                   {{Carbon\Carbon::parse($category->created_at)->diffforHumans()}}
                   @endif
                   </td>
                  <td>
                    <a href="{{url('category/edit/'.$category->id)}}" class="btn btn-info">Edit</a>
                    <a href="{{url('softdelete/category/'.$category->id)}}" class="btn btn-danger">Delete</a>
                  </td> 
               </tr>
               @endforeach
            </tbody>
          </table>  
          {{$categories->links()}}
      </div>  
    <div class="col-md-4">
      <div class="card">
        <div class="card-header">All Category</div>
     
          <div class="card-body">
              <form action="{{route('store.category')}}" method="POST">
                @csrf
                <div class="form-group">
                  <label >Category name</label>
                  <input type="text" class="form-control" name="category_name" aria-describedby="emailHelp" placeholder="Enter category">
                   @error('category_name')
                   <span class="text-danger">{{$message}}</span>

                   @enderror
                </div>
                <button type="submit" class="btn btn-primary">Add</button>
              </form>
        </div>
    </div>  
  </div>

<!-- trash part -->

  <div class="container">
    
  <div class="row">
    <div class="col-md-8">
        <div class="card-header">Trash List</div>
          <table class="table">
            <thead>
              <tr>
                <th scope="col">Sl NO</th>
                <th scope="col">category  Name</th>
                <th scope="col">User Name</th>
                <th scope="col">Created At</th>
                <th scope="col">Action</th>
              </tr>
            </thead>
            <tbody>
            <!-- @php($i=1) -->
              @foreach($trashCat as $category)
              <tr>
                <th scope="row">{{$categories->firstItem()+$loop->index}}</th>
                 <td>{{$category->category_name}}</td>
                 <td>{{$category->user->name}}</td>
                 <td>
                   @if($category->created_at==NULL)
                   <span class="text-danger">No Date Set</span>
                   @else
                   {{Carbon\Carbon::parse($category->created_at)->diffforHumans()}}
                   @endif
                   </td>
                  <td>
                    <a href="{{url('category/restore/'.$category->id)}}" class="btn btn-info">Restore</a>
                    <a href="{{url('pdelete/category/'.$category->id)}}" class="btn btn-danger">Delete</a>
                  </td> 
               </tr>
               @endforeach
            </tbody>
          </table>  
          {{$trashCat->links()}}
      </div>  
        <div class="col-md-4">
       
        </div>
    </div>  
  </div>
</div>    


  
</div>    

</x-app-layout>
