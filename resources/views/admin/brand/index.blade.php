@extends('admin.admin_master')
@section('admin')

<div class="container">
        @if(session('success'))
          <div class="alert alert-success" role="alert">
              <strong>{{session('success')}}</strong>
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
          </div>
        @endif
  <div class="row">
    <div class="col-md-8">
        <div class="card-header">All Brands</div>
          <table class="table">
            <thead>
              <tr>
                <th scope="col">Sl NO</th>
                <th scope="col">Brand  Name</th>
                <th scope="col">Brand Image</th>
                <th scope="col">Created At</th>
                <th scope="col">Action</th>
              </tr>
            </thead>
            <tbody>
            <!-- @php($i=1) -->
              @foreach($brands as $brand)
              <tr>
                <th scope="row">{{$brands->firstItem()+$loop->index}}</th>
                 <td>{{$brand->brand_name}}</td>
                 <td><img src="{{asset($brand->brand_image)}}" style="height:40px;width:70px;" ></td>
                 <td>
                   @if($brand->created_at==NULL)
                   <span class="text-danger">No Date Set</span>
                   @else
                   {{Carbon\Carbon::parse($brand->created_at)->diffforHumans()}}
                   @endif
                   </td>
                  <td>
                    <a href="{{url('brand/edit/'.$brand->id)}}" class="btn btn-info">Edit</a>
                    <a href="{{url('brand/delete/'.$brand->id)}}" onclick="return confirm('Are you sure to delete')"
                     class="btn btn-danger">Delete</a>
                  </td> 
               </tr>
               @endforeach
            </tbody>
          </table>  
          {{$brands->links()}}
      </div>  
    <div class="col-md-4">
      <div class="card">
        <div class="card-header">All brand</div>
     
          <div class="card-body">
              <form action="{{route('store.brand')}}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="form-group">
                  <label >Brand name</label>
                  <input type="text" class="form-control" name="brand_name"  placeholder="Enter brand">
                   @error('brand_name')
                   <span class="text-danger">{{$message}}</span>

                   @enderror
                </div>
                <div class="form-group">
                  <label >Brand name</label>
                  <input type="file" class="form-control" name="brand_image"  placeholder="Upload file">
                   @error('brand_image')
                   <span class="text-danger">{{$message}}</span>

                   @enderror
                </div>
                <button type="submit" class="btn btn-primary">Add Brand</button>
              </form>
        </div>
    </div>  
  </div>
  
</div>    

@endsection
